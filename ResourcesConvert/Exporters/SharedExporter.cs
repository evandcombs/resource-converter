﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace ResourcesConvert.Exporters
{
    public class SharedExporter : IExporter
    {
        public string ResourcePrefix => "//";
        public string ResourceExtension => "";
        public string SaveToFolder => "//Shared";
        public string GetStringMethod => "GetStringPartial(name, ref value);";
        public string Dependencies => "using System;";
        public string HelperCode => "";

        public string GetResourceString(string key, string value) => "";

        public StringBuilder FormatData(StringBuilder data) => data;
    }
}
