﻿using System.Text;
using System.Xml;

namespace ResourcesConvert.Exporters
{
    public class AndroidExporter : IExporter
    {
        public string ResourcePrefix => "//strings-";
        public string ResourceExtension => ".xml";
        public string SaveToFolder => "//Android";
        public string GetStringMethod => "value = Context.Resources.GetString(Context.Resources.GetIdentifier(name, \"string\", Context.PackageName));";
        public string Dependencies => "using Android.Content;";
        public string HelperCode => "public static Context Context { get; set;}";

        public string GetResourceString(string key, string value) => $"    <string name=\"{key}\">{value}</string>";

        public StringBuilder FormatData(StringBuilder data)
        {
            data.Insert(0, "<?xml version=\"1.0\" encoding=\"UTF - 8\"?>");
            data.Insert(1, "<resources>");
            data.AppendLine("</resources>");
            return data;
        }
    }
}
