﻿using System.Text;

namespace ResourcesConvert.Exporters
{
    public class iOSExporter : IExporter
    {
        public string ResourcePrefix => "//Localizable_";
        public string ResourceExtension => ".strings";
        public string SaveToFolder => "//iOS";
        public string GetStringMethod => "value = LanguageBundle.LocalizedString(name, null);";
        public string Dependencies => "using Foundation;";
        public string HelperCode => "public static NSBundle LanguageBundle { get; set; } = NSBundle.MainBundle;";

        public string GetResourceString(string key, string value) => $"\"{key}\"=\"{value}\";";

        public StringBuilder FormatData(StringBuilder data) => data;
    }
}
