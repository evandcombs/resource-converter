﻿using ResourcesConvert.Dynamics;
using ResourcesConvert.Exporters;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows.Input;

namespace ResourcesConvert.Views
{
    public class MainWindowViewModel : INotifyPropertyChanged
    {
        private const string EXTENSION = ".xrc";

        public MainWindowViewModel()
        {
            Properties = new ObservableCollection<string>();
        }

        private string CurrentProject { get; set; }

        private string namespaceString;
        public string ProjectNamespace
        {
            get => namespaceString;
            set
            {
                namespaceString = value;
                OnPropertyChanged("NamespaceString");
            }
        }

        #region Items
        private ObservableCollection<dynamic> items;
        public ObservableCollection<dynamic> Items
        {
            get => items ?? (items = DefaultItems());
            set
            {
                items = value;
                OnPropertyChanged("Items");
            }
        }

        private dynamic selectedItem;
        public dynamic SelectedItem
        {
            get => selectedItem;
            set
            {
                selectedItem = value;
                OnPropertyChanged("SelectedItem");
            }
        }

        private ObservableCollection<dynamic> DefaultItems()
        {
            DynamicType.ToDefaultType();
            ObservableCollection<dynamic> defaultItems = new ObservableCollection<dynamic>();
            defaultItems.Add(DynamicType.NewResource);
            return defaultItems;
        }

        private ObservableCollection<dynamic> RefreshItems()
        {
            ObservableCollection<dynamic> temp = new ObservableCollection<dynamic>();
            foreach (dynamic item in Items)
            {
                temp.Add(DynamicType.ToFreshType(item));
            }
            return temp;
        }
        #endregion

        #region Properties
        public ObservableCollection<string> Properties { get; set; }

        private int selectedPropertyIndex;
        public int SelectedProperyIndex
        {
            get => selectedPropertyIndex;
            set
            {
                selectedPropertyIndex = value;
                OnPropertyChanged("SelectedPropertyIndex");
            }
        }

        private string newPropertyName;
        public string NewPropertyName
        {
            get => newPropertyName;
            set
            {
                newPropertyName = value;
                OnPropertyChanged("NewPropertyName");
            }
        }

        private void AddProperty()
        {
            if (!string.IsNullOrEmpty(NewPropertyName))
            {
                DynamicType.AddProperty(new Property(NewPropertyName, typeof(string)));
                Items = RefreshItems();
                Properties.Add(NewPropertyName);
                NewPropertyName = string.Empty;
            }
        }

        private void DeleteProperty()
        {
            if (SelectedProperyIndex >= 0)
            {
                //Add two so neither Name or Default properties are deleted
                DynamicType.RemoveProperty(SelectedProperyIndex + 2);
                Items = RefreshItems();
                Properties.RemoveAt(SelectedProperyIndex);
                SelectedProperyIndex = -1;
            }
        }
        #endregion

        #region Commands
        private ICommand newCommand;
        public ICommand NewCommand
        {
            get => newCommand ?? (newCommand = new CommandHandler { Action = () => Items = DefaultItems(), IsExecutable = true });
        }

        private ICommand openCommand;
        public ICommand OpenCommand
        {
            get => openCommand ?? (openCommand = new CommandHandler { Action = OpenProject, IsExecutable = true });
        }
        private void OpenProject()
        {
            CurrentProject = FileManager.OpenFileLocator(false, EXTENSION)[0];

            (ObservableCollection<dynamic> items, string namespaceString) result = Load.ConvertableResource(CurrentProject);
            Items = result.items;
            ProjectNamespace = result.namespaceString;

            //skip the first two because we do not want them to be deleted
            for (int i = 2; i < DynamicType.PropertyNames.Count; i++)
            {
                Properties.Add(DynamicType.PropertyNames[i]);
            }
        }

        private ICommand saveCommand;
        public ICommand SaveCommand
        {
            get => saveCommand ?? (saveCommand = new CommandHandler { Action = SaveProject, IsExecutable = true });
        }
        private void SaveProject()
        {
            if (string.IsNullOrEmpty(CurrentProject))
            {
                CurrentProject = FileManager.SaveFileLocator(EXTENSION);
            }
            ConvertableExporter.Save(CurrentProject, ProjectNamespace, Items);
        }

        private ICommand exportCommand;
        public ICommand ExportCommand
        {
            get => exportCommand ?? (exportCommand = new CommandHandler { Action = ExportProject, IsExecutable = true });
        }
        private void ExportProject()
        {
            string folderPath = FileManager.OpenDirectoryLocator();

            Exporter.Export(new AndroidExporter(), folderPath, ProjectNamespace, Items);
            Exporter.Export(new iOSExporter(), folderPath, ProjectNamespace, Items);
            Exporter.Export(new WinExporter(), folderPath, ProjectNamespace, Items);
            Exporter.Export(new SharedExporter(), folderPath, ProjectNamespace, Items);
        }

        private ICommand addRowCommand;
        public ICommand AddRowCommand
        {
            get => addRowCommand ?? (addRowCommand = new CommandHandler { Action = () => Items.Add(DynamicType.NewResource), IsExecutable = true });
        }

        private ICommand deleteRowCommand;
        public ICommand DeleteRowCommand
        {
            get => deleteRowCommand ?? (deleteRowCommand = new CommandHandler { Action = DeleteRow, IsExecutable = true });
        }
        private void DeleteRow()
        {
            if (SelectedItem != null)
            {
                Items.Remove(SelectedItem);
            }
        }

        private ICommand addColumnCommand;
        public ICommand AddColumnCommand
        {
            get => addColumnCommand ?? (addColumnCommand = new CommandHandler { Action = AddProperty, IsExecutable = true });
        }

        private ICommand deleteColumnCommand;
        public ICommand DeleteColumnCommand
        {
            get => deleteColumnCommand ?? (deleteColumnCommand = new CommandHandler { Action = DeleteProperty, IsExecutable = true });
        }
        #endregion


        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string name) => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        #endregion
    }

    public class CommandHandler : ICommand
    {
        public Action Action;
        public bool IsExecutable;

        public event EventHandler CanExecuteChanged;

        public bool CanExecute(object parameter) => IsExecutable;
        public void Execute(object parameter) => Action();
    }
}
