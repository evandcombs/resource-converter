﻿using ResourcesConvert.Dynamics;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Xml;
using System.Xml.XPath;

namespace ResourcesConvert
{
    public static class Load
    {
        public static (ObservableCollection<dynamic>, string) ConvertableResource(string filepath)
        {
            string namespaceString = string.Empty;

            XmlDocument xmlDocument = FileManager.RetrieveLocalXml(filepath);
            XPathNavigator xPathNavigator = xmlDocument.CreateNavigator();
            xPathNavigator.MoveToRoot();
            xPathNavigator.MoveToFirstChild(); //resource-list
            if (xPathNavigator.MoveToFirstAttribute())
            {
                namespaceString = xPathNavigator.Value;
                xPathNavigator.MoveToParent();
            }
            if (xPathNavigator.MoveToFirstChild()) //resource
            {
                ObservableCollection<dynamic> temp = new ObservableCollection<dynamic>();

                // Get Data from first child, convert it into a dynamic bindable type, convert dict to new dynamic bindable type, and add to temp list
                Dictionary<string, string> dict = ResourceDataToDictionary(xPathNavigator);
                DynamicType.CreateType(dict, false);
                temp.Add(DynamicType.FromDictionary(dict));

                while (xPathNavigator.MoveToNext())
                {
                    // get data from child, convert dict to new dynamic bindable type, and add to temp list
                    dict = ResourceDataToDictionary(xPathNavigator);
                    temp.Add(DynamicType.FromDictionary(dict));
                }

                return (temp, namespaceString);
            }
            return (null, namespaceString);
        }
        //creates a dictionary of values found in XML
        private static Dictionary<string, string> ResourceDataToDictionary(XPathNavigator xPathNavigator)
        {
            Dictionary<string, string> dict = new Dictionary<string, string>();

            if (xPathNavigator.MoveToFirstChild()) //children
            {
                dict.Add(xPathNavigator.Name, xPathNavigator.Value);
                while (xPathNavigator.MoveToNext())
                {
                    dict.Add(xPathNavigator.Name, xPathNavigator.Value);
                }
            }
            xPathNavigator.MoveToParent();

            return dict;
        }
    }
}
