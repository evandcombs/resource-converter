﻿using System.Reflection;
using System.Xml;
using System.Collections.Generic;
using ResourcesConvert.Dynamics;

namespace ResourcesConvert.Exporters
{
    public static class ConvertableExporter
    {
        public static void Save(string filepath, string projectNamespace, IList<dynamic> items)
        {
            XmlScript xmlScript = new XmlScript();
            XmlElement rootElement = XmlScript.XmlRoot(xmlScript, "resource-list");
            XmlScript.XmlAttribute(rootElement, "namespace", projectNamespace);
            foreach (dynamic resource in items)
            {
                XmlElement resourceElement = XmlScript.XmlElement(xmlScript, rootElement, "resource");
                foreach (PropertyInfo property in DynamicType.Properties)
                {
                    string data = property.GetValue(resource);
                    string name = property.Name;
                    XmlScript.XmlElementWithText(xmlScript, resourceElement, name, data);
                }
                rootElement.AppendChild(resourceElement);
            }
            xmlScript.Save(filepath);
        }
    }
}
