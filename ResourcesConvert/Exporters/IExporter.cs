﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ResourcesConvert.Exporters
{
    public interface IExporter
    {
        string ResourcePrefix { get; }
        string ResourceExtension { get; }
        string SaveToFolder { get; }
        string GetStringMethod { get; }
        string Dependencies { get; }
        string HelperCode { get; }

        string GetResourceString(string key, string value);
        StringBuilder FormatData(StringBuilder data);
    }
}
