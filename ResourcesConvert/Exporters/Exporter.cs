﻿using ResourcesConvert.Dynamics;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace ResourcesConvert.Exporters
{
    public class Exporter
    {
        #region Const Fields
        protected const string XML_EXTENSION = ".xml";
        protected const string STRINGS_EXTENSION = ".strings";
        protected const string RESX_EXTENSION = ".resx";
        protected const string CSHARP_EXTENSION = ".cs";
        protected const string PROPERTY_NAME = "Name";
        protected const string NAMESPACE = "[NAMESPACE]";
        protected const string CSHARP = "[CSHARP]";
        protected const string GET_STRING = "[GET STRING]";
        protected const string USING_STATEMENTS = "[USING STATEMENTS]";
        protected const string DEPENDENCIES = "[DEPENDENCIES]";
        #endregion

        public static void Export(IExporter exporter, string exportPath, string projectNamespace, IList<dynamic> items)
        {
            IList<IDictionary<string, string>> convertedItems = ResourceListToDictionaryList(items);

            IDictionary<string, StringBuilder> resourceData = CreateResourceData(exporter, convertedItems);
            StringBuilder codeData = (exporter is SharedExporter) ? CreateCodeData(exporter, projectNamespace, convertedItems) : CreateCodeData(exporter, projectNamespace);

            SaveFiles(exporter, exportPath, resourceData, codeData);
        }

        private static IList<IDictionary<string, string>> ResourceListToDictionaryList(IList<dynamic> resources)
        {
            //converts resources into dictionaries for easier manipulation
            IList<IDictionary<string, string>> list = new List<IDictionary<string, string>>();
            foreach (dynamic resource in resources)
            {
                list.Add(DynamicType.ToDictionary(resource));
            }
            return list;
        }

        private static IDictionary<string, StringBuilder> CreateResourceData(IExporter exporter, IList<IDictionary<string, string>> items)
        {
            Dictionary<string, StringBuilder> data = new Dictionary<string, StringBuilder>();
            DynamicType.PropertyNames.ForEach((name) => {
                if (name != PROPERTY_NAME)
                {
                    data.Add(name, CreateResourceFile(exporter, items, name));
                }
            });
            return data;
        }

        private static StringBuilder CreateResourceFile(IExporter exporter, IList<IDictionary<string, string>> resources, string language)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("");

            foreach (IDictionary<string, string> dictionary in resources)
            {
                string key = dictionary[PROPERTY_NAME];
                foreach (KeyValuePair<string, string> pair in dictionary)
                {
                    if (pair.Key == language)
                    {
                        sb.AppendLine(exporter.GetResourceString(key, pair.Value));
                        break;
                    }
                }
            }

            return sb;
        }

        private static StringBuilder CreateCodeData(IExporter exporter, string projectNamespace)
        {
            StringBuilder sb = new StringBuilder();

            Uri uri = new Uri("csharp_strings.txt", UriKind.RelativeOrAbsolute);
            using (Stream stream = System.Windows.Application.GetResourceStream(uri).Stream)
            {
                using (StreamReader streamReader = new StreamReader(stream))
                {
                    sb.Append(streamReader.ReadToEnd());
                }
            }

            sb.Replace(DEPENDENCIES, exporter.HelperCode);
            sb.Replace(NAMESPACE, projectNamespace);
            sb.Replace(GET_STRING, exporter.GetStringMethod);
            sb.Replace(USING_STATEMENTS, exporter.Dependencies);

            return sb;
        }

        private static StringBuilder CreateCodeData(IExporter exporter, string projectNamespace, IList<IDictionary<string, string>> items)
        {
            StringBuilder sb = new StringBuilder();

            Uri uri = new Uri("csharp_strings_shared.txt", UriKind.RelativeOrAbsolute);
            using (Stream stream = System.Windows.Application.GetResourceStream(uri).Stream)
            {
                using (StreamReader streamReader = new StreamReader(stream))
                {
                    sb.Append(streamReader.ReadToEnd());
                }
            }

            sb.Replace(NAMESPACE, projectNamespace);
            sb.Replace(USING_STATEMENTS, exporter.Dependencies);

            StringBuilder csharp = new StringBuilder();
            int indent = 0;
            foreach (IDictionary<string, string> dictionary in items)
            {
                string key = dictionary[PROPERTY_NAME];
                csharp.AppendLine(Indent(indent) + "public static string " + key);
                indent = 2;
                csharp.AppendLine(Indent(indent) + "{");
                csharp.AppendLine(Indent(indent + 1) + "get");
                csharp.AppendLine(Indent(indent + 1) + "{");
                csharp.AppendLine(Indent(indent + 2) + "string value = \"\";");
                csharp.AppendLine(Indent(indent + 2) + "GetStringPartial(\"" + key + "\", ref value);");
                csharp.AppendLine(Indent(indent + 2) + "return value;");
                csharp.AppendLine(Indent(indent + 1) + "}");
                csharp.AppendLine(Indent(indent) + "}");
            }
            sb.Replace(CSHARP, csharp.ToString());

            return sb;
        }

        private static void SaveFiles(IExporter exporter, string filepath, IDictionary<string, StringBuilder> resourceFiles, StringBuilder cSharpFile)
        {
            foreach (KeyValuePair<string, StringBuilder> pair in resourceFiles)
            {
                SaveFile(filepath + exporter.SaveToFolder, exporter.ResourcePrefix + pair.Key, exporter.ResourceExtension, exporter.FormatData(pair.Value));
            }
            SaveFile(filepath + exporter.SaveToFolder, "//Strings", CSHARP_EXTENSION, cSharpFile);
        }

        private static void SaveFile(string filepath, string filename, string extension, StringBuilder data)
        {
            FileManager.CreateDirectory(filepath);
            FileManager.WriteLocalFile(filepath + filename + extension, data.ToString());
        }

        private static string Indent(int count) => new string(' ', count * 4);
    }
}
