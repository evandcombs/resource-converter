﻿using System;
using System.IO;
using System.Text;
using System.Xml;

namespace ResourcesConvert.Exporters
{
    public class WinExporter : IExporter
    {
        public string ResourcePrefix => "//strings-";
        public string ResourceExtension => ".resx";
        public string SaveToFolder => "//Win";
        public string GetStringMethod => "value = ResourceManager.GetString(\"name\", resourceCulture);";
        public string Dependencies => "using System;";
        public string HelperCode
        {
            get
            {
                StringBuilder sb = new StringBuilder();

                Uri uri = new Uri("csharp_strings_dependencies_win.txt", UriKind.RelativeOrAbsolute);
                using (Stream stream = System.Windows.Application.GetResourceStream(uri).Stream)
                {
                    using (StreamReader streamReader = new StreamReader(stream))
                    {
                        sb.Append(streamReader.ReadToEnd());
                    }
                }

                return sb.ToString();
            }
        }

        public string GetResourceString(string key, string value) => $"<data name=\"{key}\" xml:space=\"preserve\"><value>{value}</value></data>";

        public StringBuilder FormatData(StringBuilder data)
        {
            XmlScript xmlScript = new XmlScript();
            Uri uri = new Uri("/WinResource.xml", UriKind.RelativeOrAbsolute);
            using (Stream stream = System.Windows.Application.GetResourceStream(uri).Stream)
            {
                xmlScript.Load(stream);
            }

            //Add StringBuilder Lines to XML
            XmlDocumentFragment fragment = xmlScript.CreateDocumentFragment();
            fragment.InnerXml = data.ToString();
            xmlScript.DocumentElement.AppendChild(fragment);

            return new StringBuilder(xmlScript.ToString());
        }
    }
}
